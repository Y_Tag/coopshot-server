const mongoose = require("mongoose");
require("./Album");
require("./User");

const photoSchema = new mongoose.Schema({
  url: String, //URL of cloudinary
  public_id: String, //ID of the image - double check
  album: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Album",
    required: true,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  private: { type: Boolean, default: false },
  timestamp: Number,
  coords: {
    latitude: Number,
    longitude: Number,
    altitude: Number,
  },
});

module.exports = mongoose.model("Photo", photoSchema);
