const express = require("express");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const upload = require("../utils/multer");
const router = express.Router();
const User = mongoose.model("User");
const Album = mongoose.model("Album");
const Photo = mongoose.model("Photo");
const { cloudinary } = require("../utils/cloudinary");

// request for getting all users in database -> array with all emails
router.get("/user/all", async function (req, res) {
  const userlist = await User.find({}).select("email _id phone");
  res.send(userlist);
});

router.get("/user/findemailbyid", async function (req, res) {
  const userId = req.query.userId;
  const specificUserName = await User.findById(
    mongoose.Types.ObjectId(userId)
  ).select("email -_id");
  res.send(specificUserName);
});

router.get("/user/finduserbyid", async function (req, res) {
  const userId = req.user._id;
  const specificUser = await User.findById(userId)
    .select("email name imageUrl phone -_id")
    .catch((error) => {
      res.status(500).send(error.message);
    });
  if (!specificUser) res.status(404).send("User not found");
  else res.send(specificUser);
});

router.put(
  "/user/edituser",
  upload.single("profilepic"),
  async function (req, res) {
    const photo = req.file;
    const userId = req.user._id;
    const specificUser = await User.findById(userId).catch((error) => {
      res.status(500).send(error.message);
    });
    if (!specificUser) res.status(404).send("User not found");
    // if photo was uploaded
    if (photo) {
      const response = await cloudinary.uploader.upload(photo.path, {
        folder: "profilepic",
      });
      specificUser.imageUrl = response.url;
    }
    // verify before assign the new params
    if (req.body.name) specificUser.name = req.body.name;
    if (req.body.phone) specificUser.phone = req.body.phone;
    if (req.body.email) specificUser.email = req.body.email;

    await specificUser.save();
    res.status(200).send(specificUser);
  }
);

//Return All favorites albums for user
router.get("/user/getAllFav", async (req, res) => {
  // get the album name from the request data
  const userObjID = mongoose.Types.ObjectId(req.user._id);

  var user = await User.findOne({ _id: userObjID });
  if (!user) {
    //Send error msg
    res.status(400).json({
      message: "User not found",
    });
    return;
  }
  const fav = [];
  for (var i = 0; i < user.favorite.length; i++) {
    const album = await Album.findOne({
      _id: mongoose.Types.ObjectId(user.favorite[i]),
    });
    if (!album) return res.status(404).send("Album not found");
    for (var j = 0; j < album.photos.length; j++) {
      var photo = await Photo.findOne({ _id: album.photos[j] });
      if (
        !photo.private ||
        (photo.private === true &&
          photo.author._id.toString() === userObjID._id.toString())
      )
        album.photos[j] = photo;
    }
    fav.push(album);
  }
  res.send(fav);
});

router.post("/user/updateFav", async (req, res) => {
  // get the album name from the request data
  const albumID = req.body.albumID;
  const userObjID = mongoose.Types.ObjectId(req.user._id);

  var album = await Album.findOne({
    _id: mongoose.Types.ObjectId(albumID),
  });
  if (!album) return res.status(404).send("Album not found");

  var user = await User.findOne({ _id: userObjID });
  if (!user) {
    //Send error msg
    res.status(400).json({
      message: "User not found",
    });
    return;
  }
  const index = user.favorite.indexOf(mongoose.Types.ObjectId(albumID));
  if (index > -1) {
    user.favorite.splice(index, 1);
    album = null;
  } else {
    user.favorite.push(mongoose.Types.ObjectId(albumID));
    for (var j = 0; j < album.photos.length; j++) {
      var photoUrl = await Photo.findOne({ _id: album.photos[j] }).select([
        "url",
      ]);
      album.photos[j] = photoUrl;
    }
  }
  await user
    .save()
    .catch((err) => res.status(422).send({ error: err.message }));

  res.send(album);
});

module.exports = router;
