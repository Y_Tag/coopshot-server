const express = require("express");
const mongoose = require("mongoose");
const requireAuth = require("../middlewares/requireAuth");

// Imports the Google Cloud client library
const vision = require("@google-cloud/vision");

const Album = mongoose.model("Album");
const Photo = mongoose.model("Photo");

const router = express.Router();

router.use(requireAuth);

// Method for getting all albums name, id and last 5 pics given userID - search in album name and partners array
router.get("/album/all", async (req, res) => {
  const userObjID = mongoose.Types.ObjectId(req.user._id); // create ObjectId with the user id
  const albums = await Album.find({
    // find the albums in the database
    $or: [
      {
        // regex expression to search in album'name the userId
        name: { $regex: req.user._id, $options: "i" },
      },
      { partners: userObjID }, // serach in partners array the userId
    ],
  })
    .populate("photos")
    // catch the error and send back the response with error
    .catch((err) => res.status(422).send({ error: err.message }));

  if (!albums) return res.status(404).send({ error: "Album not found" });

  for (album of albums) {
    const arr = [];
    album.photosQuantity = album.photos.length;
    if (!album.photos) return;
    //Filter the relevant photos - album`s public photos or user`s private photos
    var filteredPhotos = album.photos.filter((photo) => {
      //Todo - check the method with verified DB, currently the images doesn`t have `private`
      if (
        !photo.private ||
        (photo.private === true &&
          photo.author._id.toString() === userObjID._id.toString())
      )
        return true;
      else false;
    });
    // for (let photo of album.photos) {
    //   await Photo.findOne({ _id: photo._id }).then((photoObj) =>
    //     arr.push(photoObj)
    //   );
    //   // break;
    // }
    album.photos = filteredPhotos;
  }
  res.send(albums);
});

// Method for creating new album without photos
router.post("/album/create", async (req, res) => {
  const { name, partners } = req.body;

  if (!name) {
    return res.status(422).send({ error: "You must provide a name" });
  }

  try {
    const album = new Album({ name, partners });
    await album.save();
    res.send(album);
  } catch (err) {
    res.status(422).send({ error: err.message });
  }
});

// Method for getting specific album with all photos
router.get("/album/single", async (req, res) => {
  // get the album name from the request data
  const albumID = req.query.albumId;
  const userID = req.user.id;

  if (!albumID || typeof albumID !== "string") {
    //check if request has album name or isnt a string
    return res
      .status(422)
      .send({ error: "You must provide a proper album ID" });
  }
  // search for the album in mongodb
  // const album = await Album.where('_id').equals(albumID);
  // if (!album) {
  //   return res.status(404).send({ error: 'Album not found' });
  // }
  const album = await Album.findOne({ _id: mongoose.Types.ObjectId(albumID) })
    .populate("photos partners")
    .catch((err) => res.status(500).send("Error getting the album: " + err));

  if (!album) return res.status(404).send("Album not found");

  //Filter the relevant photos - album`s public photos or user`s private photos
  var filteredPhotos = album.photos.filter((photo) => {
    //Todo - check the method with verified DB, currently the images doesn`t have `private`
    if (
      !photo.private ||
      (photo.private === true && photo.author._id.toString() === userID)
    )
      return true;
    else false;
  });
  album.photos = filteredPhotos;
  res.send(album);
  // return reponse with specific album information
  // res.send(album);
});

router.put("/album/addpartners", async (req, res) => {
  // Implement this method for invite people to album
  const albumID = req.body.albumid;
  const users = req.body.users;
  // check the request parameters if not empty
  if (!albumID || !users) {
    return res
      .status(422)
      .send({ error: "You must provide a albumID and usersIDs" });
  }
  // verify the album ID is string and the users parmeter is Array
  if (typeof albumID !== "string" || !Array.isArray(users)) {
    return res
      .status(422)
      .send({ error: "You must provide proper a albumID and usersIDs" });
  }

  try {
    const albumObjID = mongoose.Types.ObjectId(albumID); // create ObjectId with the album id

    // find the album given id
    const album = await Album.findOne({ _id: albumObjID }).catch(
      (err) => res.status(500).send({ error: err.message }) // catch the error and send back the response with error
    );
    // if album not found
    if (!album) {
      return res.status(404).send({ error: "Album not found" });
    }
    //loop through array of userIDs to add to album
    for (let userID of users) {
      const userObjID = mongoose.Types.ObjectId(userID); // create ObjectId with the user id
      album.partners.addToSet(userObjID); // Adds userObjID to the array if not already present.
    }
    await album.save(); // save the changes
  } catch (err) {
    console.error(err);
    return res.status(500).send("Something went wrong");
  }
  res.send("Users added successfully");
});

//Get request - getting all the partners of specific album
router.get("/album/partners", async (req, res) => {
  // get the album id from the request data
  const albumID = req.query.albumId;

  if (!albumID || typeof albumID !== "string") {
    //check if request has album name or isnt a string
    return res
      .status(422)
      .send({ error: "You must provide a proper album ID" });
  }
  // search for the album in mongodb
  const partners = await Album.findOne({ _id: albumID }).select(
    "partners -_id"
  );
  if (!partners) {
    return res.status(404).send({ error: "Album not found" });
  }
  // return reponse with array of all partners in the album
  res.send(partners);
});

router.put("/album/addpartner", async (req, res) => {
  // Implement this method for invite people to album

  const albumID = req.body.albumid;
  const user = req.body.user;
  // check the request parameters if not empty
  if (!albumID || !user) {
    return res
      .status(422)
      .send({ error: "You must provide a albumID and usersIDs" });
  }
  // verify the album ID is string
  if (typeof albumID !== "string") {
    return res
      .status(422)
      .send({ error: "You must provide proper a albumID and user ID" });
  }

  try {
    const albumObjID = mongoose.Types.ObjectId(albumID); // create ObjectId with the album id

    // find the album given id
    const album = await Album.findOne({ _id: albumObjID }).catch(
      (err) => res.status(500).send({ error: err.message }) // catch the error and send back the response with error
    );
    // if album not found
    if (!album) {
      return res.status(404).send({ error: "Album not found" });
    }
    //loop through array of userIDs to add to album

    const userObjID = mongoose.Types.ObjectId(user); // create ObjectId with the user id
    album.partners.addToSet(userObjID); // Adds userObjID to the array if not already present.

    await album.save(); // save the changes
  } catch (err) {
    console.error(err);
    return res.status(500).send("Something went wrong");
  }
  res.send("User was added successfully");
});

router.put("/album/removePartner", async (req, res) => {
  const partnerID = req.body.partnerID;
  const albumID = req.body.albumID;

  const updated = await Album.updateOne(
    { _id: albumID },
    { $pull: { partners: partnerID } }
  );

  if (updated.nModified) {
    return res.status(200).send("successfully removed");
  } else return res.status(404).send({ error: "Album not found" });
});

// Method for getting specific album with all photos
router.put("/album/edit", async (req, res) => {
  // get the album name from the request data
  const albumID = req.body.albumId;
  const albumName = req.body.albumName;
  const albumDesc = req.body.albumDesc;

  if (
    !albumID ||
    typeof albumID !== "string" ||
    !albumName ||
    typeof albumName !== "string" ||
    !albumDesc ||
    typeof albumDesc !== "string"
  ) {
    //check if request has album name or isnt a string
    return res
      .status(422)
      .send({ error: "You must provide a proper variables" });
  }
  var album = await Album.findOne({
    _id: mongoose.Types.ObjectId(albumID),
  }).catch((err) => res.status(500).send("Error getting the album: " + err));

  if (!album) return res.status(404).send("Album not found");
  album.name = album.name.split("~")[0] + "~" + albumName;
  album.description = albumDesc;
  await album.save();
  res.send(album);
});

module.exports = router;
