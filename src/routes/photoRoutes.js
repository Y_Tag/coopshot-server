const express = require("express");
const mongoose = require("mongoose");
const requireAuth = require("../middlewares/requireAuth");
const sstk = require("shutterstock-api");
const Photo = mongoose.model("Photo");
const UserTag = mongoose.model("UserTag");
const Tag = mongoose.model("Tag");

const imageToBase64 = require("image-to-base64");
const checkIfTagExist = require("../utils/checkIfTagExist");

const router = express.Router();

router.use(requireAuth);

async function findtags(url) {
  sstk.setAccessToken(
    "v2/RWFhTUpHcGI4cW1HWmNoMm56RUtSV3NoVDRValZBNEEvMzAwNjAzODU3L2N1c3RvbWVyLzQvUFltN1F0S2duNnYtMHZhSzlnaTRTMTlwa0QyckVSVHFEMUI1LVFMbERJU29RRkhMZWVBMlZWM1VuLUdqaEs0UlJVTzhTOUN6Y21RZ0xnaGpYNXFjSDlLMTJ3QVFsVFUwZ0YtLXgwOTgxeXdlcEN5YlpORVJxdDRDbXl2Tno1dnU0eGMyTWJnRlRrOHBPTkgxN2E1cE4wWXkwUC1TcTh4Q3RWblpCTWRGTEowaHBwUkZDUzJYNDNtVnFBVjdHTXc1TDRwWXBIRk5NTzdmWmVhcWxheG9jdy9jRTBCYmpfMm1yVWhpZl9QejBIa2Zn"
  );

  const computerVisionApi = new sstk.ComputerVisionApi();

  const base64File = await imageToBase64(url) // Image URL
    .catch((error) => {
      console.log(error); // Logs an error if there was one
    });

  const body = new sstk.ImageCreateRequest(base64File);

  const labels = await computerVisionApi
    .uploadImage(body)
    .then((data) => {
      return computerVisionApi.getKeywords(data.upload_id);
    })
    .catch((error) => {
      console.error(error);
    });
  if (labels && labels.data) return labels.data;
  else return [];
}

async function setTags(labels, photoId, userID) {
  var tags = [];
  for (const label of labels) {
    let existTag = await checkIfTagExist(label);
    if (!existTag) {
      // if tag doesn't exist create it'
      const createdTag = new Tag({
        type: label,
        photos: [photoId],
      });
      tags = [...tags, createdTag._id];
      await createdTag.save();
    } else {
      // if tag is exist add the Photo it to photos array
      existTag.photos = [...existTag.photos, photoId];
      tags = [...tags, existTag._id];
      await existTag.save();
    }
  }
  return tags;
}

router.get("/photos", async (req, res) => {
  // get the album name from the request data
  const albumID = mongoose.Types.ObjectId(req.query.albumId); // create ObjectId with the user id
  const userID = req.user.id;

  const photos = await Photo.find({
    //find the photos that connected to this album
    album: albumID,
  }).catch((err) => res.status(422).send({ error: err.message })); // catch the error and send back the response with error

  if (!photos) {
    return res.status(404).send({ error: "photos not found" });
  }

  //Filter the relevant photos - album`s public photos or user`s private photos
  const filteredPhotos = photos.filter((photo) => {
    //Todo - check the method with verified DB, currently the images doesn`t have `private`
    !(
      !photo.private ||
      (photo.private === true && photo.author._id === userID)
    );
  });

  res.send(photos);
  // res.send(filteredPhotos);
});
// Method for getting specific photo with all photos
router.put("/photo/private", async (req, res) => {
  // get the photo name from the request data
  const photoID = req.body.photoID;
  const photoPrivate = req.body.photoPrivate;

  if (
    !photoID ||
    typeof photoID !== "string" ||
    typeof photoPrivate !== "boolean"
  ) {
    //check if request has photo Id isnt a string
    return res
      .status(422)
      .send({ error: "You must provide a proper variables" });
  }

  var photo = await Photo.findOne({
    _id: mongoose.Types.ObjectId(photoID),
  }).catch((err) => res.status(500).send("Error getting the photo: " + err));

  if (!photo) return res.status(404).send("Photo not found");

  if (!photo.author.equals(req.user._id))
    //check if the user is the author
    return res
      .status(401)
      .send({ error: "You not authorized to change photo" });

  photo.private = photoPrivate;
  await photo.save();
  res.send(photo);
});

router.put("/photo/settags", async (req, res) => {
  // add tags to photo
  const userID = req.user._id;
  //Get the request data
  const { photoId } = req.body;

  if (!photoId || !mongoose.isValidObjectId(photoId)) {
    return res.status(422).send("Something went wrong");
  }

  //Get the user
  var photo = await Photo.findOne({ _id: photoId });
  if (!photo) {
    //Send error msg
    res.status(400).json({
      message: "Photo not found",
    });
    return;
  }

  try {
    const labels = await findtags(photo.url);

    photo.tags = await setTags(labels, photo._id);
    await photo
      .save()
      .catch((err) => res.status(422).send({ error: err.message }));

    //Find all the tags per specific user
    var allUserTagList = await UserTag.find({ userID: userID }).select(
      "tagID -_id"
    );
    var idUserTagList = [];
    allUserTagList.forEach((x) => idUserTagList.push(x.tagID.toString()));
    photo.tags.forEach(async (tagItem) => {
      //if tag exist in the userTagList quantity +1
      if (idUserTagList.includes(tagItem.toString())) {
        var tagUserTag = await UserTag.findOne({
          tagID: mongoose.Types.ObjectId(tagItem),
        });
        tagUserTag.quantity = tagUserTag.quantity + 1;
        await tagUserTag.save().then(() => {});
      } else {
        // -> new tag , quantity = 1
        var newUserTag = new UserTag({
          userID: userID,
          tagID: tagItem._id,
          quantity: 1,
        });
        newUserTag.save().then(() => {});
      }
    });

    res.status(200).json({
      message: "photo updated",
    });
  } catch (err) {
    console.error(err);

    for (let tagId of photo.tags) {
      var tag = await Tag.findOne({
        _id: mongoose.Types.ObjectId(tagId),
      });
      if (tag)
        tag.photos = tag.photos.filter(
          (photoId) => photoId != photo._id.toString()
        );

      var tagUserTag = await UserTag.findOne({
        tagID: mongoose.Types.ObjectId(tagId),
      });
      if (tagUserTag) tagUserTag.quantity = tagUserTag.quantity - 1;
      await tag.save();
      await tagUserTag.save();
    }

    res.status(500).json({ err: "Something went wrong", photo });
  }
});

module.exports = router;
